from django.shortcuts import render,redirect, reverse
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth import login,authenticate
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm
from django.contrib.auth.decorators import login_required


@login_required(login_url='login')
def home(request):
    # if not request.user.is_authenticated:
    #     return redirect('login')
    receipt_object = Receipt.objects.filter(purchaser__username=request.user.username)

    context = {"receipt_object": receipt_object}

    return render(request, "receipts/receipts.html", context)

def category_list(request):
    receipt_object =ExpenseCategory.objects.filter(owner__username=request.user.username)
    context = {"receipt_object": receipt_object}
    return render(request, "receipts/categories.html",context)

def account_list(request):
    receipt_object =Account.objects.filter(owner__username=request.user.username)
    context = {"receipt_object": receipt_object}
    return render(request, "receipts/accounts.html",context)

@login_required(login_url='login')
def create_expense(request):
    if request.method == "POST":
        expense_form = ExpenseForm(request.POST)
        if expense_form.is_valid():
            new_expense = expense_form.save(commit=False)
            new_expense.owner = request.user
            new_expense.save()
            return redirect(category_list)
    else:
        form = ExpenseForm
    context ={
        "form":form
    }

    return render(request,"receipts/create_expense.html", context)


@login_required(login_url='login')
def create_account(request):
    if request.method == "POST":
        account_form = AccountForm(request.POST)
        if account_form.is_valid():
            new_account = account_form.save(commit=False)
            new_account.owner = request.user
            new_account.save()
            return redirect(account_list)
    else:
        form = AccountForm
    context ={
        "form":form
    }

    return render(request,"receipts/create_account.html", context)


# class ExpenseCategoryListView(ListView):
#     model = ExpenseCategory
#     context_object_name = 'categories'
#     template_name = 'receipts/category_list.html'
#     queryset = ExpenseCategory.objects.annotate(num_receipts=Count('receipts'))




@login_required(login_url='login')
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form_receipt = form.save(commit=False)
            form_receipt.purchaser = request.user
            form_receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm

    context = {
        "form":form
    }
    return render(request, "receipts/create.html",context)
