from django.urls import path
from accounts.views import logout_view, signup, user_login

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", signup, name="signup"),
]
